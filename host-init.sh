#!/bin/bash

function forge_install(){

	echo -n "Forge version to use: "
	read server_version

	mkdir /opt/gamehost/
	cd /opt/gamehost
	
	echo "Downloading Installer..."
	wget https://files.minecraftforge.net/maven/net/minecraftforge/forge/"${server_version}"/forge-"${server_version}"-installer.jar

	echo "Installing Forge Environment..."
	java -jar forge-"${server_version}"-installer.jar --installServer
	rm forge-"${server_version}"-installer.jar
	rm forge-"${server_version}"-installer.jar.log

	java -jar forge-"${server_version}"-*.jar

	sed -i 's/eula=false/eula=true/g' eula.txt

	touch run.sh
	echo "#!/bin/bash" >> run.sh
	echo "cd /opt/gamehost" >> run.sh
	echo "command='java -Xms1024M -Xmx3096M -jar /opt/gamehost/$(ls | grep ${server_version})'" >> run.sh
	echo "screen -dmS forge \$command" >> run.sh
	chmod +x run.sh

	forge_service

	echo " == Installation Completed == "
	echo "Be sure to copy mods into the appropriate folder and edit the server.properties file "\
	"to match server use requirements."
	
	exit 0

}

function forge_service(){

	echo "Generating systemd unit..."
	cat <<-'EOF' > /etc/systemd/system/forge.service
	[Unit]
	Description="Forge Minecraft Server"
	After=network.target
	
	[Service]
	Type=oneshot
	ExecStart=/bin/bash /opt/gamehost/run.sh
	ExecStop=/usr/bin/killall java
	RemainAfterExit=true
	
	[Install]
	WantedBy=multi-user.target
	EOF

	systemctl enable forge.service

}

function main(){

	echo "Performing System Upgrade..."
	sleep 1	
	apt update
	apt full-upgrade -y
	apt autoremove --purge -y
	apt clean -y

	echo "Installing JDK & JRE..."
	sleep 1
	apt install openjdk-8-jdk openjdk-8-jre -y

	echo "Select Server Type: "
	echo "1) Forge"
	echo -n "Select (1-1): "
	read server_type
	
	case ${server_type} in
		1)
			forge_install
			;;
	esac

}

if ! [[ $USER == "root" ]]; then
	
	echo "Run as root, bitch."
	echo
	exit 1

fi

main

